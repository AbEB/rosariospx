<?php
/**
 * Letters_Import
 *
 * @package Students
 */

DrawHeader( "Importation d'un modèle" );

echo '<form action="Modules.php?modname=Students/includes/Letters_Import_Upload.php" method="post" enctype="multipart/form-data">' .
         _( 'Choisir le modèle.' ) . '<br>' .
        '<input type="file"   name="template_import" id="template_import"><br>' .
        '<input type="submit" name="submit" value=' . _( 'Importer' ) . '>'.
    '</form>' ;