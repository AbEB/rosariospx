<?php
/**
 * Letters_Import_Upload
 *
 * @package Students
 */
 
 DrawHeader( "Importation d'un modèle" );

require_once 'ProgramFunctions/MarkDownHTML.fnc.php';
require_once 'ProgramFunctions/Template.fnc.php';

    $chemin_fichier = $_FILES['template_import']['tmp_name'];
    
    if (is_uploaded_file($chemin_fichier))
    {
        $ressource = fopen($chemin_fichier, 'r');
        $taille_fichier = filesize($chemin_fichier);
        
        $contenu_fichier = fread($ressource, $taille_fichier);
        
        if (FALSE === $contenu_fichier)
        {
            echo "Le fichier n'a pu être lu.";
        }
        else
        {
            $REQUEST_letter_text = SanitizeHTML( $contenu_fichier );
            SaveTemplate( $contenu_fichier, 'Students/Letters.php' );
            echo "OK";
        }
        
        fclose($ressource);
        
        echo "<script>document.location.href='Modules.php?modname=Students/Letters.php&modfunc=&search_modfunc=list&next_modname=Students/Letters.php&advanced=&';</script>";
    }
    else
    {
        echo "Le fichier n'a pas été chargé.";
    }
    
?>