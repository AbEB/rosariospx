<?php
/**
 * Functions
 *
 * @Divers plugins
 */

// Translate plugin name.
dgettext( 'EDM_Lettres', 'Etoile' );

/**
 * Bouton import et export dans le module Student>Lettres
 */

add_action( 'Students/Letters.php|import', 'ImportExportModeleLettre' );

function ImportExportModeleLettre( )
{
    global $_SESSION, $extra;
    $extra['header_right'] .= button( 'download', _( 'Export' ), 'Modules.php?modname=Students/includes/Letters_Export.php&_ROSARIO_PDF=true target="_blank"', 'bigger' ).
    button( 'down', _( 'Import' ), 'Modules.php?modname=Students/includes/Letters_Import.php', 'bigger' ) ;
    
    $_SESSION['Template_Letters'] = GetTemplate('Students/Letters.php'); //TODO EDM Actualiser avant d'exporter.
}

/**
 * Style des étiquettes élèves
 */

add_action( 'Students/StudentsLabels.fnc.php|styleAdresse', 'MailingStyleEtoileAdresse' );

function MailingStyleEtoileAdresse ()
{
    echo '<style>
        
		table {
		    border: 0px;
		    border-collapse: collapse;
            table-layout:fixed;
            width:104%;
            cellspacing: 0;
            font-size: 14pt;
		}
        
		td.etiquette {
            padding-left:7mm;
            padding-right:2mm;
            vertical-align:middle;
            text-align: left;
            height: 178.5px;
		}
        
		</style>';
}

add_action( 'Students/StudentsLabels.fnc.php|styleEleve', 'MailingStyleEtoileEleve' );

function MailingStyleEtoileEleve ()
{
    echo '<style>
        
		table {
		    border: 0px;
		    border-collapse: collapse;
            table-layout:fixed;
            width:104%;
            cellspacing: 0;
            font-size: 16pt;
		}
        
		td.etiquette {
            padding-left:7mm;
            padding-right:2mm;
            vertical-align:middle;
            text-align: center;
            text-transform: uppercase;
            font-weight: bold;
            height: 178.5px;
		}
        
		</style>';
}