<?php
/**
 * Grades functions
 */

/**
 * Get GPA or Total row
 * Used by TranscriptsGenerate() & ReportCardsGenerate().
 *
 * @example $grades_RET[$i + 1] = GetGpaOrTotalRow( $student_id, $grades_total, $i, $show['grades_or_total'] );
 *
 * @since 5.0 Add GPA or Total.
 * @since 11.4 Deprecate $courses_number param
 *
 * @param int   $student_id     Student ID.
 * @param array $grades_total   Grades total points for each MP.
 * @param int   $courses_number Number of courses (rows). Deprecated.
 * @param bool  $percent        Show Percent grade.
 *
 * @return array GPA or Total row.
 */
function GetGpaOrTotalRow( $student_id, $grades_total, $course_number, $mp_array, $mode = 'gpa' )
{
	if ( ! is_array( $grades_total ) )
	{
		return [];
	}

	$gpa_row = [
		'COURSE_TITLE' => '<B>Moy. générale</B>',//( $mode === 'total' ? _( 'Total' ) : _( 'GPA' ) ), // TODO Carnet
		'COURSE_PERIOD_ID' => ( $mode === 'total' ? '-2' : '-1' ),
	];

	foreach ( (array) $grades_total as $mp => $grades_total_mp )
	{
		$gpa_row[$mp] = '';

		if ( $mode === 'total' )
		{
			$gpa_row[$mp] = '<B>' . $grades_total_mp . '</B>';
		}
		elseif ( GetMP( $mp ) ) // Fix check MP exists before trying to get GPA.
		{
			$cumulative_gpa = DBGetOne( "SELECT CUM_WEIGHTED_GPA
				FROM transcript_grades
				WHERE STUDENT_ID='" . (int) $student_id . "'
				AND MARKING_PERIOD_ID='" . (int) $mp . "'" );

			$gpa_row[$mp] = '<B>' . number_format( $cumulative_gpa, 2, ',', '' ) . '</B>'; // /' .
//				(float) SchoolInfo( 'REPORTING_GP_SCALE' ); // TODO EMD Carnet
		}

		if ( ! empty( $_REQUEST['elements']['minmax_grades'] ) )
		{
		    $gpa_row[$mp] = '<div class="grade-minmax-wrap"><div class="grade-minmax-min"> &nbsp; </div>
                              <div class="grade-minmax-grade">' . 
        		              $gpa_row[$mp] .
        		              '</div>';
			
            $gpa_row[$mp] .= '<div class="grade-minmax-max">' . GetClassRankRow(
                			    $student_id,
                			    $mp_array
                                )[$mp] .
                                '</div>';
                                
		}
	}
	
	return $gpa_row;
}


/**
 * Get Class Rank row
 * Used by ReportCardsGenerate().
 *
 * @example $grades_RET[$i + 4] = GetClassRankRow( $student_id, $mp_array );
 *
 * @since 8.0 Add Class Rank row.
 *
 * @param int   $student_id Student ID.
 * @param array $mp_array   Marking Periods.
 *
 * @return array Class Rank row.
 */
function GetClassRankRow( $student_id, $mp_array )
{
	if ( ! $mp_array )
	{
		return [];
	}

	$mp_list = "'" . implode( "','", $mp_array ) . "'";

	$class_rank_RET = DBGet( "SELECT MARKING_PERIOD_ID,
		CLASS_SIZE,CUM_RANK
		FROM transcript_grades
		WHERE SYEAR='" . UserSyear() . "'
		AND MARKING_PERIOD_ID IN(" . $mp_list . ")
		AND STUDENT_ID='" . (int) $student_id . "'" );

	$class_rank_row = [
		'COURSE_PERIOD_ID' => '-4',
		'COURSE_TITLE' => _( 'Class Rank' ),
	];

	foreach ( $class_rank_RET as $class_rank )
	{
		if ( ! $class_rank['CUM_RANK'] )
		{
			continue;
		}

		$mp_id = $class_rank['MARKING_PERIOD_ID'];

		$class_rank_row[ $mp_id ] = $class_rank['CUM_RANK'] . '&thinsp;/&thinsp;' . $class_rank['CLASS_SIZE'];

		if ( ! empty( $_REQUEST['elements']['minmax_grades'] ) )
		{
		    $class_rank_row[ $mp_id ] = $class_rank_row[ $mp_id ] ;
			//$class_rank_row[ $mp_id ] = '<div class="center">' . $class_rank_row[ $mp_id ] . '</div>';
		}
	}

	return $class_rank_row;
}

/**
 * Get Class Average row
 * Used by ReportCardsGenerate().
 *
 * @since 10.7 Add Class Average row.
 *
 * @example $grades_RET[$i + 3] = GetClassAverageRow( $course_periods );
 *
 * @uses GetClassAveragePercent()
 *
 *
 * @param array $course_periods Course Periods array, with MPs array.
 *
 * @return array Class Rank row.
 */
function GetClassAverageRow( $course_periods, $minmax=NULL )
{   
    static $class_averages = [];

	foreach ( (array) $course_periods as $course_period_id => $mps )
	{
		$cp_list[] = $course_period_id;

		foreach ( (array) $mps as $mp )
		{
			$mp_list[$mp[1]['MARKING_PERIOD_ID']] = $mp[1]['MARKING_PERIOD_ID'];
		}
	}

	$mp_list = "'" . implode( "','", $mp_list ) . "'";

	$cp_list = "'" . implode( "','", $cp_list ) . "'";

	$class_average_row = [
		'COURSE_PERIOD_ID' => '-3',
		//'COURSE_TITLE' => _( 'Class average' ),
	];

	if ( ! isset( $class_averages[$cp_list][$mp_list] ) )
	{
		$credits = $class_average = [];

		foreach ( (array) $course_periods as $course_period_id => $mps )
		{
			foreach ( (array) $mps as $mp )
			{
				$mp_id = $mp[1]['MARKING_PERIOD_ID'];

				if ( ! isset( $class_average[ $mp_id ] ) )
				{
					$class_average[ $mp_id ] = $credits[ $mp_id ] = 0;
				}

				$cp_credits = DBGetOne( "SELECT CREDITS
					FROM course_periods
					WHERE COURSE_PERIOD_ID='" . (int) $course_period_id . "'" );

				$class_average[ $mp_id ] += GetClassAveragePercent( $course_period_id, $mp_id ) * $cp_credits;

				$credits[ $mp_id ] += $cp_credits;
			}
		}

		foreach ( $class_average as $mp_id => $class_av )
		{
			$class_av = $class_av / $credits[ $mp_id ];

			$class_av = ( $class_av / 100 ) * SchoolInfo( 'REPORTING_GP_SCALE' );

			$class_av = number_format( $class_av, 2, ',', '' ) ;// /' . TODO Carnet
				//(float) SchoolInfo( 'REPORTING_GP_SCALE' );

			$class_average_row[ $mp_id ] = $class_av;

			if ( $minmax == 'MinMax' ) // TODO Carnet Min et max de classe
			{
			    $class_average_row[ $mp_id ] =
								AddMinMaxClasseEDM($course_periods, $mp_id, 'min') .
								$class_average_row[ $mp_id ] .
								AddMinMaxClasseEDM($course_periods, $mp_id, 'max') ;
			}
		}

		$class_averages[$cp_list][$mp_list] = $class_average_row;
	}
	
	return $class_averages[$cp_list][$mp_list];
}

/**
 * Get Class average
 * String formatted for display
 *
 * @since 9.1
 *
 * @uses GetClassAveragePercent()
 * @uses _makeLetterGrade()
 *
 * @example GetClassAverage( $course_period_id, $_REQUEST['mp'], ProgramConfig( 'grades', 'GRADES_DOES_LETTER_PERCENT' ) )
 *
 * @param int $course_period_id  Course Period ID.
 * @param int $marking_period_id Marking Period ID.
 * @param int $letter_or_percent Letter or percent or both grades.
 *
 * @return string Empty if no grades yet, else formatted average HTML (letter grade in bold followed by percent grade).
 */
function GetClassAverage( $course_period_id, $marking_period_id, $letter_or_percent = 0 )
{
	$percent_average = GetClassAveragePercent( $course_period_id, $marking_period_id );

	if ( ! $percent_average )
	{
		return '';
	}

	$percent = number_format( $percent_average, 1, '.', '' ) . '%';

	if ( $letter_or_percent > 0 )
	{
		return $percent;
	}

	require_once 'ProgramFunctions/_makeLetterGrade.fnc.php';

	$grade = _makeLetterGrade( ( $percent_average / 100 ), $course_period_id );
	
	$search =  array( '.00', '.50', '.' );
	$replace = array( ''   , ',5' , ',' );
	
	$grade = str_replace($search, $replace, $grade) ;

	if ( $letter_or_percent < 0 )
	{
	    return '<div class="grey">' . $grade . '</div>';
	}

	return '<b>' . $grade . '</b> ' . $percent;
}

/**
 * Get Class average percent
 * Get raw percent value
 *
 * @since 9.1
 * @since 11.0 Cache Class average percent
 *
 * @param int $course_period_id  Course Period ID.
 * @param int $marking_period_id Marking Period ID.
 *
 * @return float Percent average.
 */
function GetClassAveragePercent( $course_period_id, $marking_period_id )
{
	static $percent_averages = [];

	if ( isset( $percent_averages[ $course_period_id ][ $marking_period_id ] ) )
	{
		return $percent_averages[ $course_period_id ][ $marking_period_id ];
	}

	$extra['SELECT_ONLY'] = "sg1.GRADE_PERCENT";

	$extra['FROM'] = ",student_report_card_grades sg1,course_periods rc_cp";

	$extra['WHERE'] = " AND sg1.MARKING_PERIOD_ID='" . (int) $marking_period_id . "'
		AND rc_cp.COURSE_PERIOD_ID='" . (int) $course_period_id . "'
		AND rc_cp.COURSE_PERIOD_ID=sg1.COURSE_PERIOD_ID
		AND sg1.STUDENT_ID=ssm.STUDENT_ID
		AND sg1.GRADE_PERCENT IS NOT NULL";

	$students_RET = GetStuList( $extra );

	if ( ! $students_RET )
	{
		return 0.0;
	}

	$total_percent = $grades_i = 0;

	foreach ( $students_RET as $grade )
	{
		$grades_i++;

		$total_percent += $grade['GRADE_PERCENT'];
	}

	$percent_average = $total_percent / $grades_i;

	$percent_averages[ $course_period_id ][ $marking_period_id ] = $percent_average;

	return $percent_average;
}


/**
 * Get Class Rank (for Course Period)
 * Used by ReportCardsGenerate().
 *
 * @example $grades_RET[$i][$mp] .= '<br />' . GetClassRank( $student_id, $course_period_id, $mp );
 *
 * @since 11.0
 *
 * @param int   $student_id Student ID.
 * @param int   $course_period_id  Course Period ID.
 * @param int   $marking_period_id Marking Period ID.
 * @param bool  $add_class_size    Display Class Size. Optional, defaults to true.
 *
 * @return string Class Rank.
 */
function GetClassRank( $student_id, $course_period_id, $marking_period_id, $add_class_size = true )
{
	$class_rank_RET = DBGet( "SELECT sg.STUDENT_ID,sg.COURSE_PERIOD_ID,mp.MARKING_PERIOD_ID,
	(SELECT COUNT(*)+1
		FROM student_report_card_grades sg2
		WHERE sg2.GRADE_PERCENT>sg.GRADE_PERCENT
		AND sg2.MARKING_PERIOD_ID=mp.MARKING_PERIOD_ID
		AND sg2.COURSE_PERIOD_ID=sg.COURSE_PERIOD_ID
		AND sg2.STUDENT_ID IN (SELECT DISTINCT sg3.STUDENT_ID
			FROM student_report_card_grades sg3,student_enrollment se2
			WHERE sg3.STUDENT_ID=se2.STUDENT_ID
			AND sg3.MARKING_PERIOD_ID=mp.MARKING_PERIOD_ID
			AND sg3.COURSE_PERIOD_ID=sg.COURSE_PERIOD_ID
			AND sg3.SYEAR=mp.SYEAR)) AS CLASS_RANK,
	(SELECT COUNT(*)
		FROM student_report_card_grades sg4
		WHERE sg4.MARKING_PERIOD_ID=mp.MARKING_PERIOD_ID
		AND sg4.COURSE_PERIOD_ID=sg.COURSE_PERIOD_ID
		AND sg4.STUDENT_ID IN (SELECT DISTINCT sg5.STUDENT_ID
			FROM student_report_card_grades sg5,student_enrollment se3
			WHERE sg5.STUDENT_ID=se3.STUDENT_ID
			AND sg5.MARKING_PERIOD_ID=mp.MARKING_PERIOD_ID
			AND sg5.COURSE_PERIOD_ID=sg.COURSE_PERIOD_ID
			AND sg5.SYEAR=mp.SYEAR)) AS CLASS_SIZE
	FROM student_enrollment se,student_report_card_grades sg,marking_periods mp
	WHERE se.STUDENT_ID=sg.STUDENT_ID
	AND se.STUDENT_ID='" . (int) $student_id . "'
	AND sg.MARKING_PERIOD_ID=mp.MARKING_PERIOD_ID
	AND mp.MARKING_PERIOD_ID='" . (int) $marking_period_id . "'
	AND sg.COURSE_PERIOD_ID='" . (int) $course_period_id . "'
	AND se.SYEAR=mp.SYEAR
	AND sg.SYEAR=mp.SYEAR
	AND NOT sg.GRADE_PERCENT IS NULL" );

	if ( ! $class_rank_RET )
	{
		return '';
	}

	$class_rank = $class_rank_RET[1]['CLASS_RANK'];

	if ( $add_class_size )
	{
		$class_rank .= ' / <i>' . $class_rank_RET[1]['CLASS_SIZE'] . '</i>';
	}

	return $class_rank;
}

/**
 * Get the finals grades for each marking period of the quarter. RCT
 * @param unknown $course_period_id
 * @param unknown $marking_period_id
 * @param unknown $current_RET : students and notes and weight and date
 */
function GetFinalGradesQuarter( $course_period_id, $marking_period_id, $student_id )
{
    $mps_children = GetChildrenMP( 'PRO', $marking_period_id ) ;
    $mps_children = $marking_period_id .','. str_replace("'","",$mps_children) ;
    $mps = explode(",", $mps_children);

    foreach ( $mps as $mp )
    {         
		$average_RET = GetAverageCourseStudent( $course_period_id, $mp, $student_id ) ;

		UpdateAverageCourseStudent( $average_RET ) ;
    }
}

/**
 * Get the final grade for one student, one course, one marking_period RCT
 * TODO EDM
 * @param unknown $course_period_id
 * @param unknown $marking_period_id
 * @param unknown $current_RET
 * @return array
 */
function GetAverageCourseStudent( $course_period_id, $marking_period_id, $student_id)
{

        $sql = "SELECT g.STUDENT_ID,g.ASSIGNMENT_ID,g.POINTS,a.WEIGHT,a.DUE_DATE,
                t.ASSIGNMENT_TYPE_ID,t.FINAL_GRADE_PERCENT,
                c.TITLE, cp.GRADE_SCALE_ID, DOES_CLASS_RANK AS CLASS_RANK,c.CREDIT_HOURS ";
        
        $sql.= "FROM gradebook_grades g,gradebook_assignments a,gradebook_assignment_types t, courses c, course_periods cp ";
        
        $sql.= "WHERE a.ASSIGNMENT_ID=g.ASSIGNMENT_ID
                    AND t.ASSIGNMENT_TYPE_ID = a. ASSIGNMENT_TYPE_ID
        			AND g.COURSE_PERIOD_ID= $course_period_id
                    AND cp.COURSE_PERIOD_ID= $course_period_id
                    AND c.course_id = cp.course_id
                    AND g.STUDENT_ID=$student_id ";
        
        $sql.= "AND a.DUE_DATE <= ( SELECT END_DATE
					FROM school_marking_periods
					WHERE MARKING_PERIOD_ID= $marking_period_id)
				AND a.DUE_DATE >= ( SELECT START_DATE
					FROM school_marking_periods
					WHERE MARKING_PERIOD_ID= $marking_period_id) ";
        
        $notes_RET = DBGet( DBQuery( $sql ) );
        
        $average_RET= array();
        
        foreach ( $notes_RET as $id => $note )
        {
            $type = $note['ASSIGNMENT_TYPE_ID'];
            if ( $note['POINTS'] != -1 && $note['POINTS'] != -2 && $note['POINTS'])
            {
                $average_RET[$type]['SumNotes']  += $note['POINTS'] * $note['WEIGHT'] ;
                $average_RET[$type]['SumWeight'] += $note['WEIGHT'] ;
                $average_RET[$type]['TypeWeight'] = $note['FINAL_GRADE_PERCENT'] ;
                
                $average_RET[$type]['Average'] = $average_RET[$type]['SumNotes'] / $average_RET[$type]['SumWeight'] ;
            }

        }
        
        foreach ( $average_RET as $type => $average )
        {
            $average_RET['SumAverage'] += $average['Average'] * $average['TypeWeight'] ;
            $average_RET['SumCoef'] += $average['TypeWeight'] ;
            $average_RET['AverageG'] = $average_RET['SumAverage'] / $average_RET['SumCoef'] ;
            $average_RET['AverageG'] = round( $average_RET['AverageG'], 2 ); // TODO Grade_id?
        }
        
        $average_RET['title'] = $note['TITLE'] ;
        $average_RET['student_id'] = $note['STUDENT_ID'] ;
        $average_RET['course_period_id'] = $course_period_id ;
        $average_RET['grade_scale_id'] = $note['GRADE_SCALE_ID'] ;
        $average_RET['class_rank'] = $note['CLASS_RANK'] ;
        $average_RET['marking_period_id'] = $marking_period_id ;
        
        return $average_RET ;
}
/**
 * 
 * @param unknown $student_id
 * @param unknown $course_period_mp
 * @param unknown $marking_period
 * @param unknown $final_grade
 * @param string $grade : grade par défaut, ou percent, ou gradePercent
 */
function UpdateAverageCourseStudent( $average_RET, $grade = 'grade' )
{
        require_once 'ProgramFunctions/_makeLetterGrade.fnc.php';
        require_once 'ProgramFunctions/_makePercentGrade.fnc.php';
		require_once 'modules/Grades/includes/ClassRank.inc.php';
        
        $student_id = $average_RET['student_id'] ;
        $course_period_id = $average_RET['course_period_id'] ;
        $course_title = $average_RET['title'] ;
        $marking_period_id = $average_RET['marking_period_id'] ;
        $final_grade = $average_RET['AverageG'] ;
        $class_rank = $average_RET['class_rank'] ;
        $grade_scale_id = $average_RET['grade_scale_id'];
        
        $gp_passing = DBGet("SELECT gp_passing_value FROM report_card_grade_scales 
                        WHERE ID='" . (int) $grade_scale_id . "'")[1]['GP_PASSING_VALUE'];
        
        $grade_RET = DBGet("SELECT id,syear,title,gpa_value, break_off,unweighted_gp,grade_scale_id
                        FROM report_card_grades
            			WHERE SYEAR='" . UserSyear() . "'
            			AND SCHOOL_ID='" . UserSchool() . "'
            			AND GRADE_SCALE_ID='" . (int) $grade_scale_id . "'
                        AND title = '" . $final_grade ."'")[1];
        
        $course_RET = DBGet( "SELECT cp.COURSE_ID,c.TITLE AS COURSE_NAME,cp.TITLE,
                    	cp.GRADE_SCALE_ID, cp.CREDITS AS CREDITS,  
                    	DOES_CLASS_RANK AS CLASS_RANK,c.CREDIT_HOURS
                    	FROM course_periods cp,courses c
                    	WHERE cp.COURSE_ID=c.COURSE_ID
                    	AND cp.COURSE_PERIOD_ID='" . (int) $course_period_id . "'" ); // credit('" . (int) $course_period_id . "','" . (int) $marking_period_id . "') AS CREDITS,
        
        $where = "WHERE STUDENT_ID='" . (int) $student_id . "'
    					AND COURSE_PERIOD_ID='" . (int) $course_period_id . "'
    					AND MARKING_PERIOD_ID='" . (int) $marking_period_id . "'";
        
        $grade_exist = DBGet( "SELECT COUNT(*) FROM student_report_card_grades " . $where )[1]['COUNT'];
        
        $sql = $sep = '';
            
            if ( ! empty( $student_id ) )
            {
/*                if ( $grade == 'percent' 
                    && $columns['percent'] != '' )
                {
                    // FJ bugfix SQL error invalid input syntax for type numeric.
                    $percent = trim( $columns['percent'], '%' );
                    
                    if ( ! is_numeric( $percent ) )
                    {
                        $percent = (float) $percent;
                    }
                    
                    if ( $percent > 999.9 )
                    {
                        $percent = '999.9';
                    }
                    elseif ( $percent < 0 )
                    {
                        $percent = '0';
                    }
                    
                    if ( $columns['grade']
                        || $percent != '' )
                    {
                        $grade = ( $columns['grade'] ?
                            $columns['grade'] :
                            _makeLetterGrade( $percent / 100, $course_period_id, 0, 'ID' )
                            );
                        
                        $letter = $grades_RET[$grade][1]['TITLE'];
                        $weighted = $grades_RET[$grade][1]['WEIGHTED_GP'];
                        $unweighted = $grades_RET[$grade][1]['UNWEIGHTED_GP'];
                        
                        // FJ add precision to year weighted GPA if not year course period.
                        
                        if ( GetMP( $marking_period_id, 'MP' ) === 'FY'
                            && $course_period_mp !== 'FY' )
                        {
                            $weighted = $percent / 100 * $grades_RET[$grade][1]['GP_SCALE'];
                        }
                        
                        $scale = $grades_RET[$grade][1]['GP_SCALE'];
                        
                        $gp_passing = $grades_RET[$grade][1]['GP_PASSING_VALUE'];
                    }
                    else
                    {
                        $grade = $letter = $weighted = $unweighted = $scale = $gp_passing = '';
                    }
                    
                    $sql .= "GRADE_PERCENT='" . $percent . "'";
                    $sql .= ",REPORT_CARD_GRADE_ID='" . (int) $grade . "',GRADE_LETTER='" . $letter .
                    "',WEIGHTED_GP='" . $weighted . "',UNWEIGHTED_GP='" . $unweighted .
                    "',GP_SCALE='" . $scale . "'";
                    
                    // bjj can we use $percent all the time?  TODO: rework this so updates to credits occur when grade is changed
                    $sql .= ",COURSE_TITLE='" . DBEscapeString( $course_title ) . "'";
                    $sql .= ",CREDIT_ATTEMPTED='" . $course_RET[1]['CREDITS'] . "'";
                    $sql .= ",CREDIT_EARNED='" . ( (float) $weighted && $weighted >= $gp_passing ? $course_RET[1]['CREDITS'] : '0' ) . "'";
                    $sep = ',';
                }
*/              if ( $grade == 'grade' )
                {
                    
                    $grade = $grade_RET['ID'];
					$percent = _makePercentGrade( $grade, $course_period_id  );
                    $letter = $final_grade;
                    $weighted = $final_grade; //TODO
                    
                    // FJ add precision to year weighted GPA if not year course period.
/*                    
                    if ( GetMP( $marking_period_id, 'MP' ) === 'FY' //TODO
                        && $course_period_mp !== 'FY' )
                    {
                        $weighted = $percent / 100 * $grades_RET['GP_SCALE'];
                    }
*/                    
                    $unweighted = $grades_RET['UNWEIGHTED_GP'];
                    
                    $scale = $_SESSION['SchoolData']['REPORTING_GP_SCALE'];
					// TODO Carnet EDM sécurité pour vérification
					echo ($scale = '20.000') ? '' : 'Alerter immédiatement l abbé de Blois _ Code erreur: Grades.fnc.php' ;

					if ( !isset($grade) ){
						$grade = $percent = $letter = $weighted = $unweighted = $scale = '';
					}
                    
                    if ( $grade_exist > 0 )
                    {
                        $sql .= "GRADE_PERCENT='" . $percent . "'";
                        $sql .= ",REPORT_CARD_GRADE_ID='" . (int) $grade . "',GRADE_LETTER='" . $letter .
                                "',WEIGHTED_GP='" . $weighted . "',UNWEIGHTED_GP='" . $unweighted .
                                "',GP_SCALE='" . $scale . "'";
                        $sql .= ",COURSE_TITLE='" . DBEscapeString( $course_title ) . "'";
                        $sql .= ",CREDIT_ATTEMPTED='" . $course_RET[1]['CREDITS'] . "'";
                        $sql .= ",CREDIT_EARNED='" . ( (float) $weighted && $weighted >= $gp_passing ? $course_RET[1]['CREDITS'] : '0' ) . "'";
                        $sep = ',';
                        
                        $sql .= ",CLASS_RANK='" . $class_rank . "' ";
                        
                        $sql = "UPDATE student_report_card_grades
            					SET " . $sql . $where ;
                    }
                    else
                    {
                        $sql = "INSERT INTO student_report_card_grades (SYEAR,SCHOOL_ID,STUDENT_ID,
                			COURSE_PERIOD_ID,MARKING_PERIOD_ID,REPORT_CARD_GRADE_ID,GRADE_PERCENT,COMMENT,
                			GRADE_LETTER,WEIGHTED_GP,GP_SCALE,COURSE_TITLE,CREDIT_ATTEMPTED,
                			CREDIT_EARNED,CLASS_RANK,CREDIT_HOURS)
                			VALUES('" .
                			UserSyear() . "','" . UserSchool() . "','" . $student_id . "','" .
                			$course_period_id . "','" . $marking_period_id . "','" . $grade . "','" . $percent . "','" .
                			$columns['comment'] . "','" . $letter . "','" . $weighted . "','" .
                			$scale . "','" . DBEscapeString( $course_title ) . "','" .
                			$course_RET[1]['CREDITS'] . "','" .
                			( (float) $weighted && $weighted >= $gp_passing ? $course_RET[1]['CREDITS'] : '0' ) . "','" .
                			$class_rank . "'," .
                			( is_null( $course_RET[1]['CREDIT_HOURS'] ) ? 'NULL' : $course_RET[1]['CREDIT_HOURS'] ) . ")";
                    }

                    if ( $sql )
                    {
                        DBQuery( $sql );
                    }
                }
            }
/*                elseif ( isset( $columns['percent'] )
                    || isset( $columns['grade'] ) )
                {
                    $percent = $grade = '';
                    $sql .= "GRADE_PERCENT=NULL";
                    // FJ bugfix SQL bug 'NULL' instead of NULL.
                    //$sql .= ",REPORT_CARD_GRADE_ID=NULL,GRADE_LETTER=NULL,WEIGHTED_GP='NULL',UNWEIGHTED_GP='NULL',GP_SCALE='NULL'";
                    $sql .= ",REPORT_CARD_GRADE_ID=NULL,GRADE_LETTER=NULL,WEIGHTED_GP=NULL,
					UNWEIGHTED_GP=NULL,GP_SCALE=NULL";
                    $sql .= ",COURSE_TITLE='" . DBEscapeString( $course_title ) . "'";
                    $sql .= ",CREDIT_ATTEMPTED='" . $course_RET[1]['CREDITS'] . "'";
                    $sql .= ",CREDIT_EARNED='0'";
                    $sep = ',';
                }
                else
                {
                    $percent = $current_RET[$student_id][1]['GRADE_PERCENT'];
                    $grade = $current_RET[$student_id][1]['REPORT_CARD_GRADE_ID'];
                }
*/
        // @since 4.7 Automatic Class Rank calculation.
     //   ClassRankCalculateAddMP();
//         TODO Carnet Vérifier


}

function rctUpdateFinalGradesQuarter( $course_period, $user_mp )
{
	$students = DBGet( "SELECT DISTINCT g.STUDENT_ID
				FROM gradebook_grades g,gradebook_assignments a
				WHERE a.ASSIGNMENT_ID=g.ASSIGNMENT_ID
				AND a.MARKING_PERIOD_ID='" . UserMP() . "'
				AND g.COURSE_PERIOD_ID='" . UserCoursePeriod() . "'"
				);

	foreach ($students as $key => $student) 
	{
	GetFinalGradesQuarter( $course_period, $user_mp, $student['STUDENT_ID'] );
	}
}
