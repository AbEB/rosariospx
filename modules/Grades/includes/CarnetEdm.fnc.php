<?php

function ConfigCarnetEdm ( $mp, $reduit = NULL )
{
    $post = array (                    // TODO Carnet EDM Configuration manuelle des carnets
        "credits" => 'Y',
        "teacher" => 'Y',
        "average" => 'Y',
        "rank" => 'Y',
        "minmax_grades" => 'Y',
        "period_absences" => 'Y',
        "last_row" => array
        (
            "0" => 'gpa',
            "1" => '',
            "2" => 'average',
            "3" => 'rank',
            "4" => '',
        ),
        "freetext" => 'Y',
    );

    $query = DBGet( DBQuery( "SELECT mp FROM school_marking_periods WHERE marking_period_id = '" . $mp ."'" ) );
    $periode = $query[1]['MP'];

    if ( $periode == 'PRO' )
    {
        $post['notes'] = 'Y';
    }
    else
    {
        $post['comments'] = 'Y';
    }
    
    if ( isset( $reduit ) )
    {
        $post['teacher'] = 'N';
        $post['header'] = 'N';
        $post['credits'] = 'N';
    }
    
    return $post;
}
/**
 * Obtenir les notes des devoirs et des leçons // TODO Carnet Fonction GetNoteEdm
 * Fonction propre à l'Étoile du Matin
 *
 * gradebook_assignment_type:   Types de devoir // TODO EDM Bloquer les types de devoirs
 * gradebook_assignment         Les devoirs avec leurs coef.
 * gradebook_grade              Les notes
 *
 * @param unknown $student_id           L'élève
 * @param unknown $course_period_id     Le cours concerné
 * @param unknown $marking_period_id    La période
 * @param unknown $ga_type              Le type: Devoir ou lecon
 */
function GetNoteEdm ( $student_id, $course_period_id, $marking_period_id, $ga_type, $average = 'notes')
{
    static $moyenne_RET, $absence_G, $absence ;
    
    $extra['SELECT'] = 'SELECT gg.student_id, gg.course_period_id, gg.assignment_id, gg.points,
                       ga.due_date, ga.weight,
                       gat.title,
                       cp.credits as credits ';
    
    $extra['FROM'] = 'FROM gradebook_grades gg
                       INNER JOIN gradebook_assignments ga ON gg.assignment_id = ga.assignment_id
                       INNER JOIN gradebook_assignment_types gat ON ga.assignment_type_id = gat.assignment_type_id
                       INNER JOIN course_periods cp ON gg.course_period_id = cp.course_period_id ';
    
    $extra['WHERE'] = "WHERE
                       gg.student_id = '".$student_id."'
                       AND gg.course_period_id = '".$course_period_id."'
                       AND gat.title = '".$ga_type."'
                       AND ga.due_date >=
                            (SELECT post_start_date FROM school_marking_periods WHERE marking_period_id = '".$marking_period_id."')
                       AND ga.due_date <=
                            (SELECT post_end_date FROM school_marking_periods WHERE marking_period_id = '".$marking_period_id."')" ;
    
    $extra = $extra['SELECT'] . $extra['FROM'] . $extra['WHERE'] ;
    
    $notes_RET = DBGet( DBQuery( $extra ) );

    $search =  array( '.00', '.50', '.' );
    $replace = array( ''   , ',5' , ',' );

    if ( $average == 'notes' )
    {
        $absence[$student_id][$ga_type] = 'N';

        foreach ( $notes_RET as $notes )
        {
            if ( isset( $notes['POINTS'] ) && $notes['POINTS'] != -1 ) 
            {
                $absence[$student_id][$ga_type] = 'Y' ;
                $absence_G[$student_id][$ga_type] = 'Y' ;
            }
            
            if ( $notes['POINTS'] == '-1.00' )
            {
                $note = 'abs.';
            }
            else
            {
                $note     = str_replace($search, $replace, $notes['POINTS']) ;
                
                $sum     += $notes['POINTS'] * $notes['WEIGHT'] ;
                $sumcoef += $notes['WEIGHT'] ;
                $moyenne_RET[$student_id][$ga_type]['moy']  = $sum / $sumcoef;
            }
            
            $liste_notes .= ' &ensp;'. $note ;
            
            if ( $notes['WEIGHT'] != '1' )
            {
                $liste_notes .='<sup style="font-size:xx-small; font-variant-position: super;"> '. $notes['WEIGHT'] .'</sup>';
            }
        }  
            if ( isset ($moyenne_RET[$student_id][$ga_type]['moy']) )
            {
                $credits = $notes['CREDITS'];
                
                $moyenne_RET[$student_id][$ga_type]['sum'] += $moyenne_RET[$student_id][$ga_type]['moy'] * $credits ;
                
                $moyenne_RET[$student_id][$ga_type]['credits'] += $credits ;            
            }
    }

        if ( $average == 'notes' && isset( $liste_notes ) )
        {
            $liste_notes = '<span style="display: inline-block">' .$liste_notes . '</span>' ;
            
            return $liste_notes ;
        }
        
        if ( $average == 'moyenne' && $absence[$student_id][$ga_type] == 'Y')
        {
            return '<b>' . str_replace($search, $replace, round( $moyenne_RET[$student_id][$ga_type]['moy'] , 2 ) ) . '</b>'  ; 
        }
        
        if ( $average == 'moyG' && $absence_G[$student_id][$ga_type] == 'Y' )
        {
            $moyG = $moyenne_RET[$student_id][$ga_type]['sum'] / $moyenne_RET[$student_id][$ga_type]['credits'] ;
            
            $moyG = '<b>' . str_replace($search, $replace, round( $moyG , 2 ) ) . '</b>' ;
            
            return $moyG ;
        }       
}

/**
 * 
 * @param unknown $student_id
 * @param unknown $marking_period_id
 * @return string
 */
function GetGeneralAverage($student_id, $marking_period_id)
{
    $student_average = DBGetOne( "SELECT CUM_WEIGHTED_GPA
            				FROM transcript_grades
            				WHERE STUDENT_ID='" . $student_id . "'
            				AND MARKING_PERIOD_ID='" . $marking_period_id . "'" ) ;
    $student_average = number_format( $student_average, 2,',','') ;
    
    return $student_average ;
}

/**
 * Entête pour les carnets de l'Étoile
 *
 * @param Image $logo_pic : le logo de l'étoile
 * @param array $student : données de l'élève
 */

function HeaderEdm ( $logo_pic, $student, $trimestre )
{
    
    echo '<table class="container">';
    
    $logo_pic = 'assets/school_logo_' . UserSchool() . '.jpg';
    
    $student_name = '<name style="font-size:larger; font-weight:bold">';
    $student_name .= $student['FIRST_NAME'].' ';
    
    if (isset($student['MIDDLE_NAME']))
    {
        $student_name .= $student['MIDDLE_NAME'].' ';
    }
    
    $student_name .= '<span style="font-size:x-large;font-variant-caps: small-caps;">';
    $student_name .= $student['LAST_NAME'] .'</span></name>';
    
    $birthday = date_create( $student['CUSTOM_200000004'] ) ;
    
    $syear = FormatSyear( UserSyear(), Config( 'SCHOOL_SYEAR_OVER_2_YEARS' ) );
    
    // Logo
    if ( file_exists( $logo_pic ) )
    {
        $picwidth = 140;
        
        echo '<tr>
                <td rowspan="3" style="width:' . $picwidth . 'px;">
						<img src="' . URLEscape( $logo_pic ) . '" width="' . AttrEscape( $picwidth ) . '" />
			    </td>
                <td rowspan="4" style="width:50px"></td>';
    }
    
    // Titre
    echo       '<td class="titre">' . 'Bulletin de notes' . '</td>
                <td style="width:50px"></td>
                <td style="width:' . $picwidth . 'px;"></td>
            </tr>
            <tr>' ;
    
    // Élève
    echo        '<td colspan="3" class="student">'. $student_name . '</td>
            </tr>';
    
    echo    '<tr>
                <td></td>
                <td colspan="2" rowspan="2" class="student2">' .
                date_format($birthday, "d/m/Y") . '</br></br>Classe de ' .
                $student['GRADE_ID'] . '</br>Année ' .
                $syear . '</br>' .
                '</td>
            </tr>';
                // Adresse de l'école
                echo    '<tr>
                <td class="address">' .
                '<span style="font-weight:bold">' . $student['SCHOOL_TITLE'] . '</span></br>' .
                $_SESSION['SchoolData']['PHONE'] . '</br>'  .
                $_SESSION['SchoolData']['WWW_ADDRESS'] .
                //$school_number[1]['ZIPCODE'] .' '. $school_number[1]['CITY'] . //'</br>' .
                //$school_number[1]['SCHOOL_NUMBER'] .
                '</td>';
                // Période
                echo    '   <td class="periode">' .
                    $trimestre .'
                </td>
            </tr>' ;
                    
                    echo '</table>';
}

/**
 *
 */
function FooterEdm ()
{
    $school = DBGet( DBQuery( " SELECT school_number, city, zipcode, phone, www_address
             FROM schools WHERE syear = '".UserSyear()."'
             AND id = '".UserSchool()."' ") );
    
    $footer = '<footer style="width: 100%; text-align: center">';
    
    $footer .=   '— ' . $school[1]['SCHOOL_NUMBER'] . ' — ' .
        $school[1]['PHONE'] . ' — ' .
        $school[1]['WWW_ADDRESS'] . ' — ' .
        '57e.etoiledumatin@fsspx.fr' . ' — ' ;
        
        $footer .= '</footer>';
        
        return $footer;
}
/**
 * Mise en forme du nom du professeur
 *
 * @param int $teacher_id :
 *
 * TODO Carnet EDM Fonction nom du professeur
 */
function TeacherNameEdm ($mps)
{
   
    if ( GetTeacher( $mps[key( $mps )][1]['TEACHER_ID'], 'TITLE' ) )
    {
        $teacher_name  = _( GetTeacher( $mps[key( $mps )][1]['TEACHER_ID'], 'TITLE' ) ). ' ';
    }
    
    $teacher_middle_name = GetTeacher( $mps[key( $mps )][1]['TEACHER_ID'], 'MIDDLE_NAME' );
    
    if ( issetVal($teacher_middle_name) ) {
        $teacher_name .= '<span style="font-variant-caps: small-caps">' .
            $teacher_middle_name  . '</span> ';
    }
    
    $teacher_name .= '<span style="font-variant-caps: small-caps">' .
        GetTeacher( $mps[key( $mps )][1]['TEACHER_ID'], 'LAST_NAME' ) .
        '</span>';
        
        return $teacher_name ;
}

/**
 * Reprise de la fonction originale. Des choses inutiles ou compliquées.
 * Il vaudrait mieux repartir de la fonction GetReportCardMinMax
 * qui a uniquement besoin de la période (sous forme de array: $cours_period et sort tous les min et max.
 * @param unknown $min_max_grades
 * @param unknown $grades_RET
 * @param unknown $LO_columns
 * @return string par défaut min, sauf si $exportmax est définie
 */
function AddReportCardMinMaxGradesEDM ( $min_max_grades, $grades_RET, $minoumax, &$LO_columns )
{
    static $columns_done = array();
    
    require_once 'ProgramFunctions/_makeLetterGrade.fnc.php';
    
    $grades_loop = $grades_RET;
    
    $search =  array( '.00', '.50', '.' );
    $replace = array( ''   , ',5' , ',' );
    
    foreach ( (array) $grades_loop as $i => $grade )
    {
        if ( empty( $grade['COURSE_PERIOD_ID'] ) )
        {
            continue;
        }
        
        $cp_id = $grade['COURSE_PERIOD_ID'];
        
        $min_max_grades_cp = issetVal( $min_max_grades[ $cp_id ] );
        
        foreach ( (array) $min_max_grades_cp as $mp_id => $min_max )
        {
            if ( ! isset( $grades_RET[$i][$mp_id] ) )
            {
                continue;
            }
            
            $min_grade = issetVal( $min_max[1]['GRADE_MIN'], '0' );
            $max_grade = issetVal( $min_max[1]['GRADE_MAX'], '0' );
            
            if ( $cp_id > 0 )
            {
                $min_grade = _makeLetterGrade( $min_grade / 100, $cp_id );
                $max_grade = _makeLetterGrade( $max_grade / 100, $cp_id );
                
                $min_grade = str_replace($search, $replace, $min_grade) ;
                $max_grade = str_replace($search, $replace, $max_grade) ;
            }
            elseif ( $cp_id === '-3' )
            {
                // CP ID=-1 is Total GPA, format float.
                $min_grade = number_format( $min_grade, 2, ',', '' );
                $max_grade = number_format( $max_grade, 2, ',', '' );
            }
            if ( ! empty( $columns_done[$mp_id] ) )
            {
                continue;
            }
            
            $LO_columns[$mp_id] = '<div class="grade-minmax-wrap">
                <div class="grade-minmax-min">' . '&nbsp;' . '</div>
				<div class="grade-minmax-grade">' . 'Moy.' . '</div>
				<div class="grade-minmax-max">' . 'Rang' . '</div></div>' . '<br>' . '
				<div class="grade-minmax-wrap"><div class="grade-minmax-min">' . '<i> Min.</i>' . '</div>
                <div class="grade-minmax-grade">' . '<i>Classe</i>' . '</div>
				<div class="grade-minmax-max">' . '<i>Max. </i>' . '</div></div>';
            
            $columns_done[$mp_id] = true;
        }
    }
    
    $min_grade = '<div class="grade-minmax-wrap"><div class="grade-minmax-min grey">' . $min_grade . '</div>
				<div class="grade-minmax-grade">';
    
    $max_grade = '</div>
				<div class="grade-minmax-max grey">' . $max_grade . '</div></div>';
    
    if ( $minoumax == 'max' )
    {
        return $max_grade;
    }
    else
    {
        return $min_grade ;
    }
}
/**
 *
 * @param unknown $course_periods
 * @param unknown $minoumax
 * @return string
 */
function AddMinMaxClasseEDM ( $course_periods, $mp, $minoumax )
{
    $min_grade = GetReportCardMinMaxGrades($course_periods)[-1][$mp][1]['GRADE_MIN'];
    $max_grade = GetReportCardMinMaxGrades($course_periods)[-1][$mp][1]['GRADE_MAX'];
    
    $min_grade = number_format( $min_grade, 2, ',', '' );
    $max_grade = number_format( $max_grade, 2, ',', '' );
    
    $min_grade = '<div class="grade-minmax-wrap"><div class="grade-minmax-min grey">' . $min_grade . '</div>
				<div class="grade-minmax-grade">';
    
    $max_grade = '</div>
				<div class="grade-minmax-max grey">' . $max_grade . '</div></div>';
    
    if ( $minoumax == 'max' )
    {
        return $max_grade;
    }
    else
    {
        return $min_grade ;
    }
}


/**
 * Daily Absences this quarter or Year-to-date Daily Absences.
 * Local function.
 *
 * @param  string $st_list              Student List
 * @param  string $last_mp              Last MP
 * @return array  $attendance_day_RET
 */
function _getAttendanceRETEdm( $st_list, $last_mp )
{
	/**
	 * @var mixed
	 */
	static $attendance_day_RET = null,
	$last_st_list,
		$last_last_mp;

	if ( ! $attendance_day_RET
		|| $last_st_list !== $st_list
		|| $last_last_mp !== $last_mp )
	{
		$extra['WHERE'] = " AND s.STUDENT_ID IN (" . $st_list . ")";

		$extra['SELECT_ONLY'] = "ad.SCHOOL_DATE,ad.MARKING_PERIOD_ID,ad.STATE_VALUE,ssm.STUDENT_ID";

		$extra['FROM'] = ",attendance_day ad";

		$extra['WHERE'] .= " AND ad.STUDENT_ID=ssm.STUDENT_ID
			AND ad.SYEAR=ssm.SYEAR
			AND (ad.STATE_VALUE='0.0' OR ad.STATE_VALUE='.5')
			AND ad.SCHOOL_DATE<='" . GetMP( $last_mp, 'END_DATE' ) . "'
            AND ad.SCHOOL_DATE>='" . GetMP( $last_mp, 'START_DATE' ) . "'";

		$extra['group'] = [ 'STUDENT_ID' ]; //, 'MARKING_PERIOD_ID'

		// Parent: associated students.
		$extra['ASSOCIATED'] = User( 'STAFF_ID' );

		$attendance_day_RET = GetStuList( $extra );
	}

	$last_last_mp = $last_mp;
	$last_st_list = $st_list;

	return $attendance_day_RET;
}

/*
*
*/
function GetMPAbsencesEdm( $st_list, $last_mp, $student_id )
{
	$attendance_day_RET = _getAttendanceRETEdm( $st_list, $last_mp );
    
	$count = 0;

	if ( isset( $attendance_day_RET[$student_id] ) )
	{
		foreach ( (array) $attendance_day_RET[$student_id] as $abs )
		{
			$count += 1 - $abs['STATE_VALUE'];
		}
	}

	return $count;
}

/**
 * Other Attendance this quarter or Other Attendance Year-to-date.
 * Local function.
 *
 * @param  string $st_list   Student List
 * @return array  Attendance RET
 */
function _getTardiesRETEdm( $st_list )
{
	/**
	 * @var mixed
	 */
	static $attendance_RET = null,
		$last_st_list;

	if ( ! $attendance_RET
		|| $last_st_list !== $st_list )
	{
		$extra['WHERE'] = " AND s.STUDENT_ID IN (" . $st_list . ")";

		$extra['SELECT_ONLY'] = "ap.SCHOOL_DATE,ap.COURSE_PERIOD_ID,ac.ID AS ATTENDANCE_CODE,
			ap.MARKING_PERIOD_ID,ssm.STUDENT_ID";

		$extra['FROM'] = ",attendance_codes ac,attendance_period ap";

		$extra['WHERE'] .= " AND ac.ID=ap.ATTENDANCE_CODE
			AND (ac.DEFAULT_CODE!='Y' OR ac.DEFAULT_CODE IS NULL)
			AND ac.SYEAR=ssm.SYEAR
			AND ap.STUDENT_ID=ssm.STUDENT_ID";

		$extra['group'] = [ 'STUDENT_ID', 'ATTENDANCE_CODE', 'MARKING_PERIOD_ID' ];

		// Parent: associated students.
		$extra['ASSOCIATED'] = User( 'STAFF_ID' );

		$attendance_RET = GetStuList( $extra );
	}

	$last_st_list = $st_list;

	return $attendance_RET;
}

/**
 * Marking Period Tardies.
 *
 * @uses _getAttendanceRET()
 * @uses _getOtherAttendanceCodes()
 *
 * @param  string $st_list     Student List
 * @param  string $last_mp     Last MP
 * @param  string $student_id  Student ID
 * @return string "[attendance code] in [last MP]: x"
 */
function GetMPTardiesEdm( $st_list, $last_mp, $student_id )
{
	// Other Attendance this quarter or Other Attendance Year-to-date.
	$attendance_RET = _getTardiesRETEdm( $st_list );
    
	// Get Other Attendance Codes.
	$other_attendance_codes = _getOtherAttendanceCodes();

	$count = 0;

	if ( ! empty( $attendance_RET[$student_id]['5'][$last_mp] ) )
	{
		foreach ( (array) $attendance_RET[$student_id]['5'][$last_mp] as $abs )
		{
			$count++;
		}
	}

	$tardies_code_title = $other_attendance_codes[$_REQUEST['mp_tardies_code']][1]['TITLE'];

	return sprintf( _( '%s in %s' ), $tardies_code_title, GetMP( $last_mp, 'TITLE' ) ) . ': ' .
		$count;
}
/* Ligne ajoutée au début de Grades.php pour afficher toutes les notes par défaut.
 * $_REQUEST['assignment_id'] = empty( $_REQUEST['assignment_id'] ) ? 'all' : $_REQUEST['assignment_id'] ;
*/
    