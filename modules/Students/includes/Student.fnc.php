<?php
/**
 * Student functions
 *
 * @package RosarioSIS
 * @subpackage modules
 */

/**
 * Student Delete SQL queries
 *
 * @since 5.2
 *
 * @param int $student_id Student ID.
 *
 * @return string Delete SQL queries.
 */
function StudentDeleteSQL( $student_id )
{
	// Do not try to delete Grades, Attendance, or Schedule records
	// in case records exist, we must keep them.
	$delete_sql = "DELETE FROM students_join_address
		WHERE STUDENT_ID='" . (int) $student_id . "';";

	$delete_sql .= "DELETE FROM students_join_people
		WHERE STUDENT_ID='" . (int) $student_id . "';";

	$delete_sql .= "DELETE FROM students_join_users
		WHERE STUDENT_ID='" . (int) $student_id . "';";

	$delete_sql .= "DELETE FROM student_enrollment
		WHERE STUDENT_ID='" . (int) $student_id . "';";

	$delete_sql .= "DELETE FROM food_service_accounts
		WHERE ACCOUNT_ID='" . (int) $student_id . "';";

	$delete_sql .= "DELETE FROM food_service_student_accounts
		WHERE STUDENT_ID='" . (int) $student_id . "';";

	$delete_sql .= "DELETE FROM students
		WHERE STUDENT_ID='" . (int) $student_id . "';";

	return $delete_sql;
}
// TODO EDM Script previous et next
?>
<script type="text/javascript">
	function get_focused ( )
	{
		var active_el = document.activeElement.id ;
		//document.getElementById("active_el").innerHTML=active_el
		alert( active_el ) ;
	}

	function next_student ( active_el )
	{
		//var active_el = dataTransfer.getData('text');
		
		//const active_el = document.activeElement.id ;
		
		//active_el.replace( actual_id, next_id );
		
		alert( 'truc' + active_el ) ;
		
		//document.getElementById('student').submit();
		
		//window.location.href = validation_url ;
		
		//window.open( next_url + '&active_el=' + active_el);
		
		//document.getElementById("active_el").focus();
		
	}
</script>
<?php

